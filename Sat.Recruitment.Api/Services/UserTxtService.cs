using Sat.Recruitment.Api.Domain;
using Sat.Recruitment.Api.Services.Interfaces;

namespace Sat.Recruitment.Api.Services;
public class UserTxtService : IUserTxtService
{
    public async Task<List<User>> GetUsersAsync()
    {
        var resp = new List<User>();
        var reader = ReadUsersFromFile();

        while (reader.Peek() >= 0)
        {
            var line = await reader.ReadLineAsync();
            var array = line!.Split(',');

            var user = new User
            {
                Name = array[0].ToString(),
                Email = array[1].ToString(),
                Phone = array[2].ToString(),
                Address = array[3].ToString(),
                UserType = array[4].ToString(),
                Money = decimal.Parse(array[5].ToString()),
            };
            resp.Add(user);
        }

        reader.Close();
        return resp;
    }

    private StreamReader ReadUsersFromFile()
    {
        var path = Directory.GetCurrentDirectory() + "/Files/Users.txt";

        FileStream fileStream = new FileStream(path, FileMode.Open);

        StreamReader reader = new StreamReader(fileStream);
        return reader;
    }
}