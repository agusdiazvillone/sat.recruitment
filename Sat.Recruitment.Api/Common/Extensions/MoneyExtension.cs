namespace Sat.Recruitment.Api.Common.Extensions;
public static class MoneyExtension
{
    public static decimal NormalizeMoneyByUserType(this string moneyAsString, string userType)
    {
        var money = decimal.Parse(moneyAsString);

        var percentage = GetPercentageByUserTypeAndMoney(money, userType);

        var gif = money * percentage;
        return money + gif;
    }

    private static decimal GetPercentageByUserTypeAndMoney(decimal money, string userType) => (userType, money) switch
    {
        { userType: "Normal", money: > 100 } => Convert.ToDecimal(0.12),
        { userType: "Normal", money: < 100 } => Convert.ToDecimal(0.8),
        { userType: "SuperUser", money: > 100 } => Convert.ToDecimal(0.20),
        { userType: "Premium", money: > 100 } => 2,
        _ => 0
    };
}