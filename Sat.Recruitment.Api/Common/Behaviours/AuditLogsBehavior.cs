using System.Reflection;
using System.Text.Json;
using MediatR;
using Sat.Recruitment.Api.Common.Attributes;

namespace Sat.Recruitment.Api.Common.Behaviours;
public class AuditLogsBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
{
    private readonly ILogger<AuditLogsBehavior<TRequest, TResponse>> _Logger;

    public AuditLogsBehavior(ILogger<AuditLogsBehavior<TRequest, TResponse>> logger)
    {
        _Logger = logger;
    }

    public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
    {
        var auditLogAttributes = request.GetType().GetCustomAttributes<AuditLogAttribute>();
        if (auditLogAttributes.Any())
            _Logger.LogInformation($"IN -> Request {request.GetType().Name} -> {JsonSerializer.Serialize(request)}");

        var result = await next();

        if (auditLogAttributes.Any())
            _Logger.LogInformation($"OUT -> Request {request.GetType().Name} -> {JsonSerializer.Serialize(result)}");

        return result;
    }
}