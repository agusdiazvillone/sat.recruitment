namespace Sat.Recruitment.Api.Domain
{
    public class User
    {
        public string Name { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string Address { get; set; } = default!;
        public string Phone { get; set; } = default!;
        public string UserType { get; set; } = default!;
        public decimal Money { get; set; }
    }
}