using Sat.Recruitment.Api.Domain;

namespace Sat.Recruitment.Api.Services.Interfaces;
public interface IUserTxtService
{
    Task<List<User>> GetUsersAsync();
}