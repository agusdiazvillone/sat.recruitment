using System.Net;
using System.Net.Http.Json;
using FluentAssertions;
using NUnit.Framework;
using Sat.Recruitment.Api.Features.Commands;

namespace Sat.Recruitment.Test.Features.Commands;

public class CreateUserCommandTest : TestBase
{

    [Test]
    public async Task ShouldCreateUser_Success()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Mike",
            Email = "mike@gmail.com",
            Address = "Av. Juan G",
            Phone = "+349 1122354215",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().NotThrow();
    }

    [Test]
    public async Task ShouldCreateSuperUser_Success()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Mike",
            Email = "mike@gmail.com",
            Address = "Av. Juan G",
            Phone = "+349 1122354215",
            UserType = "SuperUser",
            Money = "90"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().NotThrow();
    }

    [Test]
    public async Task ShouldCreatePremiumUser_Success()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Mike",
            Email = "mike@gmail.com",
            Address = "Av. Juan G",
            Phone = "+349 1122354215",
            UserType = "Premium",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().NotThrow();
    }

    [Test]
    public async Task ShouldCreateUser_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Agustina",
            Email = "Agustina@gmail.com",
            Address = "Garay y Otra Calle",
            Phone = "+534645213542",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithExistEmail_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Mike",
            Email = "Agustina@gmail.com",
            Address = "Av. Juan G",
            Phone = "+349 1122354215",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithExistPhone_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Mike",
            Email = "mike@gmail.com",
            Address = "Av. Juan G",
            Phone = "+534645213542",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithExistNameAndAddress_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Agustina",
            Email = "mike@gmail.com",
            Address = "Garay y Otra Calle",
            Phone = "+349 1122354215",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithoutName_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Email = "mike@gmail.com",
            Address = "Garay y Otra Calle",
            Phone = "+349 1122354215",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithoutEmail_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Agustina",
            Address = "Garay y Otra Calle",
            Phone = "+349 1122354215",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithoutAddress_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Agustina",
            Email = "mike@gmail.com",
            Phone = "+349 1122354215",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithoutPhone_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Agustina",
            Email = "mike@gmail.com",
            Address = "Garay y Otra Calle",
            UserType = "Normal",
            Money = "124"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Test]
    public async Task ShouldCreateUserWithoutMoney_Failure()
    {
        // Arrenge
        var client = Application!.CreateClient();

        // Act
        var command = new CreateUserCommand
        {
            Name = "Agustina",
            Email = "mike@gmail.com",
            Address = "Garay y Otra Calle",
            Phone = "+349 1122354215",
            UserType = "Normal"
        };

        var result = await client.PostAsJsonAsync("api/User", command);

        // Assert
        FluentActions.Invoking(() => result.EnsureSuccessStatusCode())
            .Should().Throw<HttpRequestException>();

        result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }


}