using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
using Sat.Recruitment.Api;

namespace Sat.Recruitment.Test;

public class ApiWebApplicationFactory : WebApplicationFactory<ApiDummyClass>
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.UseEnvironment("Testing");

        base.ConfigureWebHost(builder);
    }

    protected override IHost CreateHost(IHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            // Mock setups
        });

        return base.CreateHost(builder);
    }
}