using System.Text.Json;
using AutoMapper;
using FluentValidation;
using MediatR;
using Sat.Recruitment.Api.Common.Attributes;
using Sat.Recruitment.Api.Common.Exceptions;
using Sat.Recruitment.Api.Common.Extensions;
using Sat.Recruitment.Api.Domain;
using Sat.Recruitment.Api.Services.Interfaces;

namespace Sat.Recruitment.Api.Features.Commands;

[AuditLog]
public class CreateUserCommand : IRequest
{
    public string Name { get; set; } = default!;
    public string Email { get; set; } = default!;
    public string Address { get; set; } = default!;
    public string Phone { get; set; } = default!;
    public string UserType { get; set; } = default!;
    public string Money { get; set; } = default!;
}

public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand>
{
    private readonly IMapper _Mapper;
    private readonly IUserTxtService _UserTxtService;

    public CreateUserCommandHandler(IMapper mapper, IUserTxtService userTxtService)
    {
        _Mapper = mapper;
        _UserTxtService = userTxtService;
    }

    public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        var user = _Mapper.Map<User>(request);

        var userList = await _UserTxtService.GetUsersAsync();

        if (userList.Any(x =>
            x.Email == user.Email ||
            x.Phone == user.Phone ||
            (x.Name == user.Name && x.Address == user.Address)
        ))
            throw new AlreadyExistException(nameof(User), JsonSerializer.Serialize(request));


        return Unit.Value;
    }
}

public class CreateUserValidator : AbstractValidator<CreateUserCommand>
{
    public CreateUserValidator()
    {
        RuleFor(r => r.Name)
            .NotNull()
            .NotEmpty()
            .WithMessage("The name is required");

        RuleFor(r => r.Email)
            .NotNull()
            .NotEmpty()
            .WithMessage("The email is required");

        RuleFor(r => r.Address)
            .NotNull()
            .NotEmpty()
            .WithMessage("The address is required");

        RuleFor(r => r.Phone)
            .NotNull()
            .NotEmpty()
            .WithMessage("The phone is required");

        RuleFor(r => r.Money)
            .NotNull()
            .NotEmpty()
            .Must(x => decimal.TryParse(x, out var resp))
            .WithMessage("The money is required and must a number");
    }
}

public class CreateUserCommandMapper : Profile
{
    public CreateUserCommandMapper() =>
        CreateMap<CreateUserCommand, User>()
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email.NormalizeEmail()))
            .ForMember(dest => dest.Money, opt => opt.MapFrom(src => src.Money.NormalizeMoneyByUserType(src.UserType)));
}